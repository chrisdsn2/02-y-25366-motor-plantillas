// importar express
const express = require("express");
const db = require("./db/data");
const hbs = require("hbs");

//creamos la aplicacion express
const app = express();
const path = require('path');

app.use(express.static('public'));
app.set('view engine', 'hbs');

// se configura que se apunta a la carpeta views
app.set("views", __dirname + "/views");

//para el navegacion generica
hbs.registerPartials(__dirname + "/views/partials");

//ruta localhost:{puerto}
app.get("/", (request,response) => {
    response.render("index"),{
        home: db.home[0]
    };
    integrantes: db.integrantes// 

});

//cuando escriba la direccion http://localhost:3000/paginas/wordcloud/index.html
app.get("/wordcloud", (request,response) => {
    response.render("wordcloud");
});

//cuando escriba la direccion http://localhost:3000/paginas/info_curso/index.html
app.get("/info_curso", (request,response) => {
    response.render("info_curso");
});

// *****************************************************************
app.get("/:matricula", (request, response, next) => {
    const matricula = request.params.matricula;

    // Buscar el integrante por la matrícula
    const integranteFilter = db.integrantes.find(integrante => integrante.matricula === matricula);

    // Filtrar los medios por la matrícula
    const medios = db.media.filter(media => media.matricula === matricula);

    // Verificar si se encontró el integrante o los medios
    if (!integranteFilter || medios.length === 0) {
            //renderizar la pagina "error" si el integrante es incorrecto
            response.status(404).render("error");
    }else{
        // Renderizar la página 'integrante' con los datos filtrados
        response.render('integrantes', {
        integrante: integranteFilter,
        media: medios,
        tipomedia: db.tipomedia
        }); 
    }
});
// Ruta comodín para cualquier otra URL que no haya sido manejada
/* app.use((req, res, next) => {
    res.status(404).render("error");
});

app.get("/*", (request, response) => {
    response.status(404).send(`
        <h1>La pagina que estás buscando no existe.</h1>
        <p><a href="/">Volver al índice de inicio</a></p>
    `);
});*/ 

// comentaremos esto como prueba
//app.use(express.static(path.join(__dirname, 'public')));

// iniciar app escuchando puerto parametro
app.listen(3000, () => {
    console.log("Servidor corriendo en el puerto 3000");
});

//Se utiliza consola para que se imprima una notificacion
//console.log("base de datos simulada", db);
//console.log(db.integrantes);
//console.log(db.integrantes[0].codigo);